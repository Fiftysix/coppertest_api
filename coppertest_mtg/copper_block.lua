-- Copper block
local block_def = minetest.registered_nodes["default:copperblock"]
if block_def then

    minetest.override_item("default:copperblock", {
        paramtype2 = "color",
        palette = "coppertest_power_levels.png",
        drop = "default:copperblock",
    })

    block_def.groups.coppertest_not_solid = 1
    coppertest.register_wire("default:copperblock", {
        input_sides = coppertest.sides.all,
        output_sides = coppertest.sides.all,
        groups = block_def.groups,
        on_power_change = function(pos, node, level, strength)
            node.param2 = strength / coppertest.max_power * 255
            minetest.swap_node(pos, node)
        end
    })
end

-- Copper block slab
if minetest.registered_nodes["stairs:slab_copperblock"] then

    minetest.override_item("stairs:slab_copperblock", {
        paramtype2 = "colorfacedir",
        palette = "coppertest_power_levels.png",
        drop = "stairs:slab_copperblock",
    })
    coppertest.register_wire("stairs:slab_copperblock", {
        on_power_change = function(pos, node, level, strength)
            local color = math.floor(strength / coppertest.max_power * 7)
            node.param2 = (node.param2 % 32) + (color * 32)
            minetest.swap_node(pos, node)
        end,
        input_sides = {
            coppertest.sides.front, coppertest.sides.back,
            coppertest.sides.right, coppertest.sides.left,
            coppertest.sides.down,
        },
        output_sides = {
            coppertest.sides.front, coppertest.sides.back,
            coppertest.sides.right, coppertest.sides.left,
            coppertest.sides.down,
        },
    })
end
