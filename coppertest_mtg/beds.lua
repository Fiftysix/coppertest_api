if not beds then return end

local queue = coppertest.require("queue.lua")
local helpers = coppertest.require("helpers.lua")


local function register_coppertest_bed(bed)
    local bed_base = bed.."_bottom"

    local base_def = minetest.registered_nodes[bed_base]
    old_on_rightclick = base_def.on_rightclick

    minetest.override_item(bed_base, {
        on_rightclick = function(pos, node, clicker, ...)
            local itemstack = old_on_rightclick(pos, node, clicker, ...)

            local playername = clicker:get_player_name()
            if beds.player[playername] then
                coppertest.set_level(pos, node, coppertest.max_power)
                queue.queue(pos, 5, "node.sleeping_check_callback", pos, playername)
            end

            return itemstack
        end,
    })
    coppertest.register_component(bed_base, {
        output_sides = helpers.concat_tables(
            {coppertest.sides.back},
            coppertest.sides.read_all, coppertest.sides.read_solid_all_extend
        ),

        sleeping_check_callback = function(pos, playername)
            if beds.player[playername] then
                queue.queue(pos, 2, "node.sleeping_check_callback", pos, playername)
                return
            end
            local node = minetest.get_node(pos)
            coppertest.set_level(pos, node, 0)
        end,
    })

    coppertest.register_wire(bed.."_top", {
        output_sides = helpers.concat_tables(
            coppertest.sides.read_all, coppertest.sides.read_solid_all_extend
        ),
        input_sides = {coppertest.sides.front},
    })
end


for i, bed in pairs({"beds:fancy_bed", "beds:bed"}) do
    register_coppertest_bed(bed)
end

local old_register_bed = beds.register_bed
beds.register_bed = function(name, ...)
    old_register_bed(name, ...)
    register_coppertest_bed(name)
end
