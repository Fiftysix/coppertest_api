local helpers = coppertest.require("helpers.lua")


local function recalculate_storage_level(pos, inv_name)
    local meta = minetest.get_meta(pos)
    local list = meta:get_inventory():get_list(inv_name)

    local total = 0
    if list then
        for i, itemstack in ipairs(list) do
            total = total + itemstack:get_count() / itemstack:get_stack_max()
        end
    end

    local level = 0
    if total ~= 0 then
        level = math.floor((total / #list) * (coppertest.max_power - 1) + 1)
    end

    local node = minetest.get_node(pos)
    coppertest.set_level(pos, node, level)
end


local function register_coppertest_storage(name, inv_name)
    local def = minetest.registered_nodes[name]
    if not def then return end

    coppertest.register_component(name, {
        output_sides = helpers.concat_tables(
            coppertest.sides.read_all, coppertest.sides.read_solid_all_extend
        ),
    })

    local old_on_move = def.on_metadata_inventory_move
    local old_on_put = def.on_metadata_inventory_put
    local old_on_take = def.on_metadata_inventory_take

    -- remove coppertest group added by register_component.
    -- It is only used for wires to connect to, which we don't want in this case.
    def.groups.coppertest = 0

    minetest.override_item(name, {
        groups = def.groups,
        on_metadata_inventory_move = function(pos, from_list, from_idx, to_list, ...)
            if old_on_move then old_on_move(pos, from_list, from_idx, to_list, ...) end
            if from_list == inv_name or to_list == inv_name then
                recalculate_storage_level(pos, inv_name)
            end
        end,
        on_metadata_inventory_put = function(pos, list, ...)
            if old_on_put then old_on_put(pos, list, ...) end
            if list == inv_name then
                recalculate_storage_level(pos, inv_name)
            end
        end,
        on_metadata_inventory_take = function(pos, list, ...)
            if old_on_take then old_on_take(pos, list, ...) end
            if list == inv_name then
                recalculate_storage_level(pos, inv_name)
            end
        end,
    })
end


for i, chest in ipairs({"default:chest", "default:chest_locked"}) do
    register_coppertest_storage(chest, "main")
    register_coppertest_storage(chest.."_open", "main")
end

local old_register_chest = default.chest.register_chest
default.chest.register_chest = function(chest, ...)
    old_register_chest(chest, ...)
    register_coppertest_storage(chest, "main")
    register_coppertest_storage(chest.."_open", "main")
end

register_coppertest_storage("default:bookshelf", "books")
register_coppertest_storage("vessels:shelf", "vessels")
register_coppertest_storage("bones:bones", "main")
