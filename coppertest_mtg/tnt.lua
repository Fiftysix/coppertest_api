if not tnt then return end

local helpers = coppertest.require("helpers.lua")


local function register_coppertest_tnt(tnt, def)
    local def = def or minetest.registered_nodes[tnt]

    coppertest.register_component(tnt, {
        input_sides = helpers.concat_tables(
            coppertest.sides.all, coppertest.sides.solid_all_extend
        ),
        on_power_change = function(pos, node, level, strength)
            def.on_ignite(pos)
        end,
    })

    -- burning
    coppertest.register_component(tnt.."_burning", {
        output_sides = helpers.concat_tables(
            coppertest.sides.read_all, coppertest.sides.read_solid_all_extend
        ),
    })

    local burn_def = minetest.registered_nodes[tnt.."_burning"]
    local old_on_construct = burn_def.on_construct

    minetest.override_item(tnt.."_burning", {
        on_construct = function(pos, ...)
            old_on_construct(pos, ...)
            local node = minetest.get_node(pos)
            coppertest.set_level(pos, node, coppertest.max_power)
        end,
    })
end

for name, def in pairs(minetest.registered_nodes) do
    if def.groups.tnt then
        register_coppertest_tnt(name, def)
    end
end

local old_register_tnt = tnt.register_tnt
tnt.register_tnt = function(def, ...)
    old_register_tnt(def, ...)
    local name = def.name
    if not name:find(':') then
        name = "tnt:"..name
    end
    register_coppertest_tnt(name)
end


register_coppertest_tnt("tnt:gunpowder")
