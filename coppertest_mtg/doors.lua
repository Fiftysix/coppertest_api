if not doors then return end

local helpers = coppertest.require("helpers.lua")


-- Doors

local old_door_toggle = doors.door_toggle
doors.door_toggle = function(pos, node, ...)
    local state = minetest.get_meta(pos):get_int("state") % 2 == 1
    coppertest.set_level(pos, node, state and 0 or coppertest.max_power)
    old_door_toggle(pos, node, ...)
end

local function register_coppertest_door(door)
    coppertest.register_component(door, {
        input_sides = helpers.concat_tables(
            coppertest.sides.all, coppertest.sides.solid_all_extend
        ),
        output_sides = helpers.concat_tables(
            coppertest.sides.read_all, coppertest.sides.read_solid_all_extend
        ),
        on_power_change = function(pos, node, level, strength)
            local state = minetest.get_meta(pos):get_int("state") % 2 == 1
            if state ~= (level > 0) then
                doors.door_toggle(pos, node)
            end
        end,
    })
end

for door, v in pairs(doors.registered_doors) do
    register_coppertest_door(door)
end

local old_register_door = doors.register
doors.register = function(name, ...)
    old_register_door(name, ...)
    register_coppertest_door(name)
end

-- doors:hidden is the top half of the door.
-- TODO: make it possible to read and power top half


-- Trapdoors

local old_trapdoor_toggle = doors.trapdoor_toggle
doors.trapdoor_toggle = function(pos, node, ...)
    local state = node.name:sub(-5) == "_open"
    coppertest.set_level(pos, node, state and 0 or coppertest.max_power)
    old_trapdoor_toggle(pos, node, ...)
end

local function register_coppertest_trapdoor(trapdoor)
    coppertest.register_component(trapdoor, {
        input_sides = helpers.concat_tables(
            coppertest.sides.all, coppertest.sides.solid_all_extend
        ),
        output_sides = helpers.concat_tables(
            coppertest.sides.read_all, coppertest.sides.read_solid_all_extend
        ),
        on_power_change = function(pos, node, level, strength)
            local state = node.name:sub(-5) == "_open"
            if state ~= (level > 0) then
                doors.trapdoor_toggle(pos, node)
            end
        end,
    })
end

for trapdoor, v in pairs(doors.registered_trapdoors) do
    register_coppertest_trapdoor(trapdoor)
end

local old_register_trapdoor = doors.register_trapdoor
doors.register_trapdoor = function(name, ...)
    old_register_trapdoor(name, ...)
    register_coppertest_trapdoor(name)
end


-- Gates

local gates = {
    "doors:gate_wood",
    "doors:gate_acacia_wood",
    "doors:gate_junglewood",
    "doors:gate_pine_wood",
    "doors:gate_aspen_wood",
}

local function register_coppertest_gate(gate)
    local node_def = minetest.registered_nodes[gate]
    local old_on_rightclick = node_def.on_rightclick
    minetest.override_item(gate, {
        on_rightclick = function(pos, node, ...)
            local state = node.name:sub(-5) == "_open"
            coppertest.set_level(pos, node, state and 0 or coppertest.max_power)
            old_on_rightclick(pos, node, ...)
        end
    })

    coppertest.register_component(gate, {
        input_sides = helpers.concat_tables(
            coppertest.sides.all, coppertest.sides.solid_all_extend
        ),
        output_sides = helpers.concat_tables(
            coppertest.sides.read_all, coppertest.sides.read_solid_all_extend
        ),
        on_power_change = function(pos, node, level, strength)
            local state = node.name:sub(-5) == "_open"
            if state ~= (level > 0) then
                node_def.on_rightclick(pos, node)
            end
        end,
    })
end

for i, gate in ipairs(gates) do
    register_coppertest_gate(gate.."_open")
    register_coppertest_gate(gate.."_closed")
end

local old_register_fencegate = doors.register_fencegate
doors.register_fencegate = function(name, ...)
    old_register_fencegate(name, ...)
    register_coppertest_gate(name.."_open")
    register_coppertest_gate(name.."_closed")
end
