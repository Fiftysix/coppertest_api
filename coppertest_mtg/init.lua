if not default or not coppertest then
    minetest.log("warn", "coppertest mtg compat not loaded due to missing dependencies")
    return
end

local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)

dofile(modpath.."/copper_block.lua")
dofile(modpath.."/doors.lua")
dofile(modpath.."/beds.lua")
dofile(modpath.."/storage.lua")
dofile(modpath.."/tnt.lua")
