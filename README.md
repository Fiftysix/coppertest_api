# coppertest

A minetest mod and api for powering nodes through wires, aiming for predictable, consistent, and interesting contraptions. Inspired by minecraft's redstone, and motivated by mesecons.
