coppertest.vectors = {
    vector.new( 0, 1, 0),
    vector.new( 0,-1, 0),
    vector.new( 1, 0, 0),
    vector.new(-1, 0, 0),
    vector.new( 0, 0, 1),
    vector.new( 0, 0,-1),
}

--[[
sides = {

    up    = 0,
    down  = 1,
    left  = 2,
    right = 3,
    back  = 4,
    front = 5,

    up_up    = 6 + up * 6 + up    = 6
    up_left  = 6 + up * 6 + left  = 8
    up_right = 6 + up * 6 + right = 9
    up_back  = 6 + up * 6 + back  = 10
    up_front = 6 + up * 6 + front = 11

    down_down  = 6 + down * 6 + up  = 13
    ...
    front_front = 6 + front * 6 + front = 41

    -- the node at the first vector must be solid
    solid_up_up       = 42 + up * 6 + up    = 42
    ...
    solid_front_front = 42 + front * 6 + front = 77


    -- weak can not connect to weak
    weak_up = 78
    ...
    weak_solid_front_front = 155

    -- read can only connect to read
    read_up = 156
    ...
    read_solid_front_front = 233
}]]--


local sides = {}
coppertest.sides = sides

-- Populate sides table
local side_names = {"up", "down", "left", "right", "back", "front"}
local idx = 0
for i, category in ipairs({"", "weak_", "read_"}) do

    local all = {}
    for i, side in ipairs(side_names) do
        sides[category..side] = idx
        table.insert(all, idx)
        idx = idx + 1
    end
    sides[category.."all"] = all

    for i, prefix in ipairs({"", "solid_"}) do
        local all_extend = {}
        local all_extend_all = {}
        for i_a, side_a in ipairs(side_names) do
            for i_b, side_b in ipairs(side_names) do
                if i_a ~= i_b%2*2-1+i_b then  -- skip left_right, etc
                    sides[category..prefix..side_a.."_"..side_b] = idx

                    if  -- don't include up_right and right_up, etc if not solid
                        prefix == "solid_" or
                        not sides[category..prefix..side_b.."_"..side_a]
                    then
                        table.insert(all_extend_all, idx)
                    end
                    if i_a == i_b then
                        table.insert(all_extend, idx)
                    end
                end
                idx = idx + 1
            end
        end
        sides[category..prefix.."all_extend"] = all_extend
        sides[category..prefix.."all_extend_all"] = all_extend_all
    end
end
