local helpers = coppertest.require("helpers.lua")

local queue = {
    queue_table = {},
    queue_table_updated = false,
    defer_table = {},
}

-- Load the saved queue
local mod_storage = minetest.get_mod_storage()
queue.queue_table = {}--minetest.deserialize(mod_storage:get_string("coppertest:queue_table")) or {}
coppertest.tick_idx = mod_storage:get_int("coppertest:tick_idx")


-- Queue a callback to run in a future tick
-- Callback is a string "namespace.function" where namespace is "power" or "node"
function queue.queue(pos, delay, callback, ...)
    queue.queue_table_updated = true

    local hash = minetest.hash_node_position(pos)
    local block_pos = helpers.to_block_pos(pos)
    local block_hash = minetest.hash_node_position(block_pos)

    -- If a delay is too long, reduce it to the maximum delay
    if delay >= coppertest.queue_extent then
        minetest.log("warning", "Coppertest: callback queue dalay too high ("..
                     delay.." >= "..coppertest.queue_extent..")")
        delay = coppertest.queue_extent - 1

    -- Or if too short, increase to 1
    elseif delay < 1 then
        minetest.log("warning", "Coppertest: callback queue dalay must be at least 1")
        delay = 1
    end

    local block_table = queue.queue_table[block_hash]
    if not block_table then
        -- Set ticks table size to coppertest.queue_extent
        local ticks = {}
        for i=1, coppertest.queue_extent do
            ticks[i] = {}
        end
        ticks[delay] = {
            [hash] = {
                [callback] = {...}
            }
        }
        queue.queue_table[block_hash] = {index = 0, count = 1, ticks = ticks}
        return
    end

    local tick = (block_table.index - 1 + delay) % coppertest.queue_extent + 1
    local node_table = block_table.ticks[tick][hash]
    if not node_table then
        if #block_table.ticks[tick] == 0 then
            block_table.count = block_table.count + 1
        end
        block_table.ticks[tick][hash] = {[callback] = {...}}
        return
    end

    node_table[callback] = {...}
end


-- Defer a callback to run at the end of the current tick
-- Callback is a function
function queue.defer(pos, callback, ...)
    local hash = minetest.hash_node_position(pos)

    local node_table = queue.defer_table[hash]
    if not node_table then
        queue.defer_table[hash] = {[callback] = {...}}
        return
    end

    node_table[callback] = {...}
end


local function execute_tick()
    -- Iterate all blocks
    for block_hash, block_queue in pairs(queue.queue_table) do
        local block_pos = minetest.get_position_from_hash(block_hash)

        -- Only execute tick on loaded blocks
        if minetest.compare_block_status(block_pos, "active") then

            -- Increment tick
            block_queue.index = block_queue.index % coppertest.queue_extent + 1

            -- execute all callbacks in tick
            for hash, callbacks in pairs(block_queue.ticks[block_queue.index]) do
                for callback, args in pairs(callbacks) do
                    if string.sub(callback, 0, 6) == "power." then
                        local f_name = string.sub(callback, 7)
                        power[f_name](unpack(args))

                    elseif string.sub(callback, 0, 5) == "node." then
                        local f_name = string.sub(callback, 6)
                        local pos = minetest.get_position_from_hash(hash)
                        local node = minetest.get_node(pos)
                        local def = minetest.registered_nodes[node.name]
                        if def.coppertest and def.coppertest[f_name] then
                            def.coppertest[f_name](unpack(args))
                        end
                    end
                end
            end

            if not helpers.table_is_empty(block_queue.ticks[block_queue.index]) then
                queue.queue_table_updated = true
                block_queue.count = block_queue.count - 1
            end

            -- If all ticks are empty, delete the block
            if block_queue.count == 0 then
                queue.queue_table[block_hash] = nil

            else
                -- Clear this tick
                block_queue.ticks[block_queue.index] = {}
            end
        end
    end

    -- Execute deferred callbacks
    for hash, callbacks in pairs(queue.defer_table) do
        for callback, args in pairs(callbacks) do
            callback(unpack(args))
        end
    end

    -- Clear deferred callbacks
    queue.defer_table = {}
end


local step = 0
minetest.register_globalstep(function(dtime)
    step = step + dtime

    -- this will also check if the previous tick has finished once
    -- multithreading is implemented
    if step >= coppertest.seconds_per_tick then
        step = step % coppertest.seconds_per_tick
        coppertest.tick_idx = coppertest.tick_idx + 1

        execute_tick()

        -- Save the queue
        if queue.queue_table_updated then
            queue.queue_table_updated = false
            mod_storage:set_string("coppertest:queue_table", minetest.serialize(queue.queue_table))
            mod_storage:set_int("coppertest:tick_idx", coppertest.tick_idx)
        end
    end
end)


return queue
