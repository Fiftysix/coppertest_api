local helpers = coppertest.require("helpers.lua")
local queue = coppertest.require("queue.lua")

power = {}


-- List all nodes that connect to this one
function power.get_connections(pos_a, node_a, dir_idx, all_sides)
    local direction = ({{"out", "in"}, {"in", "out"}})[dir_idx]
    local connections = {}

    local def_a = minetest.registered_nodes[node_a.name]
    local sides_a = def_a.coppertest[direction[1].."put_sides"] or {}
    local rotation_table_a = helpers.get_rotation_table(node_a, def_a)

    -- Normal input/output_sides
    for idx, side_a in ipairs(sides_a) do
        local is_connected = false

        local cat_a, type_a, pos_b, between_a =
            helpers.get_side_info(side_a, pos_a, rotation_table_a)
        local node_b = minetest.get_node(pos_b)
        local def_b = minetest.registered_nodes[node_b.name]

        if (type_a ~= 2 or helpers.is_node_solid(between_a)) and def_b and def_b.coppertest then
            local sides_b = def_b.coppertest[direction[2].."put_sides"] or {}
            local rotation_table_b = helpers.get_rotation_table(node_b, def_b)

            -- check if side has inputs which match this node
            for i, side_b in ipairs(sides_b) do
                local cat_b, type_b, pos_c, between_b =
                    helpers.get_side_info(side_b, pos_b, rotation_table_b)

                if vector.equals(pos_c, pos_a) then
                    if
                        -- Weak
                        ((cat_a ~= 1 and cat_b ~= 1) or cat_a ~= cat_b) and
                        -- Read
                        ((cat_a ~= 2 and cat_b ~= 2) or cat_a == cat_b) and
                        -- Solid
                        (type_b ~= 2 or (type_a == 2 and vector.equals(between_a, between_b)))
                    then
                        is_connected = true
                        table.insert(connections, {
                            pos  = pos_b,
                            node = node_b,
                            def  = def_b,
                        })
                        break
                    end
                end
            end
        end
        if not is_connected and all_sides then
            table.insert(connections, false)
        end
    end

    return connections
end


-- Calls update_wire on a list of nodes. Part of the power decrease algorithm.
local function call_deferred_updates(deferred_updates)
    for i, deferred in ipairs(deferred_updates) do
        local deferred_meta = minetest.get_meta(deferred[1])

        if deferred[3].coppertest.type == "component" then
            power.update_component(deferred[1], deferred[2], deferred[3])

        elseif deferred_meta:get_int("coppertest_level") == 0 then
            power.update_wire(deferred[1], deferred[2], deferred[3])
        end
    end
end

-- Update the inputs of a component
function power.update_component(pos, node, def)
    node = node or minetest.get_node(pos)
    def = def or minetest.registered_nodes[node.name]
    local meta = minetest.get_meta(pos)

    -- Get all inputs (and non-inputs)
    local input_nodes = power.get_connections(pos, node, 2, true)

    -- We want to collect the power coming into each individual side, in the
    -- order specified by coppertest.input_sides
    local old_side_inputs = minetest.deserialize(meta:get_string("coppertest_input_sides"))
    local new_side_inputs = {}

    if old_side_inputs and #old_side_inputs ~= #input_nodes then
        old_side_inputs = nil
    end

    -- Also keep track of maximum strength and level
    local old_level = meta:get_int("coppertest_level")
    local old_strength = meta:get_int("coppertest_strength")

    local new_level = 0
    local new_strength = 0

    -- Keep track of changes so we know if to call on_power_change
    local sides_changed = false

    -- Calculate the power coming into this component
    for idx, input in ipairs(input_nodes) do
        local input_meta = input and minetest.get_meta(input.pos)
        local input_level, input_strength = 0, 0
        local input_type = input and input.def.coppertest.type

        if input_type == "wire" then
            input_level = input_meta:get_int("coppertest_level")
            input_strength = input_meta:get_int("coppertest_strength")

        elseif input_type == "component" then
            input_strength = input_meta:get_int("coppertest_output_level")
            input_level = coppertest.ignore_level and 1 or input_strength
        end

        -- Check if this side has changed
        local side_changed = false
        if (old_side_inputs and (  -- old_side_inputs might be nil
            old_side_inputs[idx].level ~= input_level or
            old_side_inputs[idx].strength ~= input_strength
        )) or (not old_side_inputs and input_level > 0) then
            sides_changed = true
            side_changed = true
        end

        -- Keep track of each side's power
        new_side_inputs[idx] = {
            level=input_level,
            strength=input_strength,
            changed=side_changed
        }

        -- Keep track of maximum power
        if (input_level > new_level and input_strength > 0) or
           (input_level == new_level and input_strength > new_strength) then
            new_level = input_level
            new_strength = input_strength
        end
    end

    -- Nothing changed
    if not sides_changed then
        return
    end

    -- Save values for future comparison
    meta:set_string("coppertest_input_sides", minetest.serialize(new_side_inputs))
    meta:set_int("coppertest_level", new_level)
    meta:set_int("coppertest_strength", new_strength)

    local max_power_changed = false
    if new_level ~= old_level or new_strength ~= old_strength then
        max_power_changed = true
    end

    -- Call node callback
    if def.coppertest.on_power_change then
        queue.defer(pos, def.coppertest.on_power_change,
                         pos, node, new_level, new_strength, max_power_changed, new_side_inputs)
    end
end


-- Algorithm to quickly handle power decreases. Part of update_wire.
local function power_decrease(meta, pos, node, def, old_level, old_strength,
                              output_nodes, deferred_updates)
    -- we set this and all dependents to 0, and then re-calculate from
    -- scratch. This bypasses the gradual dissipation of power (very slow).
    meta:set_int("coppertest_level", 0)
    meta:set_int("coppertest_strength", 0)

    if def.coppertest.on_power_change then
        queue.defer(pos, def.coppertest.on_power_change, pos, node, 0, 0)
    end

    local deferred_head = false
    if not deferred_updates then
        deferred_head = true
        deferred_updates = {{pos, node, def}}
    else
        table.insert(deferred_updates, {pos, node, def})
    end
    for i, output in ipairs(output_nodes) do
        if output.def.coppertest.type == "component" then
            table.insert(deferred_updates, {output.pos, output.node, output.def})
        else
            local output_meta = minetest.get_meta(output.pos)
            local output_level = output_meta:get_int("coppertest_level")
            local output_strength = output_meta:get_int("coppertest_strength")

            if output_level == old_level and output_strength == old_strength-1 then
                power.update_wire(output.pos, output.node, output.def, deferred_updates)
            end
        end
    end

    if deferred_head then
        call_deferred_updates(deferred_updates)
    end
end


-- Update a wire (recursive)
function power.update_wire(pos, node, def, deferred_updates)
    def = def or minetest.registered_nodes[node.name]
    if def.coppertest.type == "component" then
        minetest.debug("update_wire called on component")
        return power.update_component(pos, node, def)
    end

    local meta = minetest.get_meta(pos)

    local old_level = meta:get_int("coppertest_level")
    local old_strength = meta:get_int("coppertest_strength")

    local input_nodes = power.get_connections(pos, node, 2)

    local new_level = 0
    local new_strength = 0

    -- Calculate the power coming into this wire
    for i, input in ipairs(input_nodes) do
        local input_meta = minetest.get_meta(input.pos)
        local input_level, input_strength = 0, 0
        local input_type = input.def.coppertest.type

        if input_type == "wire" then
            input_level = input_meta:get_int("coppertest_level")
            input_strength = input_meta:get_int("coppertest_strength") - 1

        elseif input_type == "component" then
            input_strength = input_meta:get_int("coppertest_output_level")
            input_level = coppertest.ignore_level and 1 or input_strength
        end

        if (input_level > new_level and input_strength > 0) or
           (input_level == new_level and input_strength > new_strength) then
            new_level = input_level
            new_strength = input_strength
        end
    end

    -- Nothing changed
    if new_level == old_level and new_strength == old_strength then
        return
    end

    local output_nodes = power.get_connections(pos, node, 1)

    -- Power decrease
    if new_level < old_level or
       (new_level == old_level and new_strength < old_strength) then
        power_decrease(meta, pos, node, def, old_level, old_strength,
                       output_nodes, deferred_updates)

    -- Power increase
    else
        meta:set_int("coppertest_level", new_level)
        meta:set_int("coppertest_strength", new_strength)

        if def.coppertest.on_power_change then
            queue.defer(pos, def.coppertest.on_power_change,
                            pos, node, new_level, new_strength)
        end

        for i, output in ipairs(output_nodes) do
            if output.def.coppertest.type == "component" then
                power.update_component(output.pos, output.node, output.def)
            else
                local output_meta = minetest.get_meta(output.pos)
                local output_level = output_meta:get_int("coppertest_level")
                local output_strength = output_meta:get_int("coppertest_strength")

                if
                    (output_level == old_level and output_strength == old_strength-1) or
                    (output_level <  new_level and new_strength > 1) or
                    (output_level == new_level and output_strength <  new_strength-1)
                then
                    power.update_wire(output.pos, output.node, output.def)
                end
            end
        end
    end
end


function power.update_neighbours(pos, node)
    local output_nodes = power.get_connections(pos, node, 1)
    local deferred_updates = {}

    for i, output in ipairs(output_nodes) do
        if output.def.coppertest.type == "component" then
            power.update_component(output.pos, output.node, output.def)
        else
            power.update_wire(output.pos, output.node, output.def, deferred_updates)
        end
    end

    call_deferred_updates(deferred_updates)
end


-- Call update_neighbours on oldnode and newnode
function power.update_rotation(pos, oldnode, newnode)
    power.update_neighbours(pos, oldnode)
    power.update_neighbours(pos, newnode)
end


function power.set_level(pos, node, level)
    local meta = minetest.get_meta(pos)
    local current_level = meta:get_int("coppertest_output_level")
    if current_level == level and level ~= 0 then return end
    meta:set_int("coppertest_output_level", level)
    power.update_neighbours(pos, node)
end

return power
