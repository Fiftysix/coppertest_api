local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)
local __loaded = {}  -- stores require() cache

coppertest = {
    require = function(name)
        local path = modpath.."/"..name
        if __loaded[path] then
            return __loaded[path]
        end

        local module = dofile(path)
        __loaded[path] = module
        return module
    end,

    tick_idx = 0,
    ignore_level = false,  -- set all levels to 1 (minecraft-style power propogation)
    max_power = 15,
    queue_extent = 16,  -- number of ticks into the future that the queue extends
    seconds_per_tick = 0.1,  -- number of seconds per tick
}

coppertest.require("sides.lua")
local power = coppertest.require("power.lua")
local queue = coppertest.require("queue.lua")
local helpers = coppertest.require("helpers.lua")


function coppertest.set_level(pos, node, level, delay)
    delay = delay or 1
    queue.queue(pos, delay, "power.set_level", pos, node, level)
end


local function sides_to_strings(def)
    local side_conversion = {"top", "bottom", "right", "left", "back", "front"}

    if not (def.input_sides or def.output_sides) then
        return nil
    end

    local connect_sides = {}
    local function iterate_table(tbl)
        for i, side in ipairs(tbl) do
            table.insert(connect_sides, side_conversion[side+1])
        end
    end
    iterate_table(def.input_sides or {})
    iterate_table(def.output_sides or {})
    return connect_sides
end

local function get_after_place_node(node_def, definition)
    local old_after_place_node = node_def.after_place_node

    if definition.skip_place_rotation or node_def.paramtype2 ~= "facedir" or ((
        not definition.input_sides or
        helpers.table_is_empty(definition.input_sides)
    ) and (
        not definition.output_sides or
        helpers.table_is_empty(definition.output_sides)
    )) then
        return old_after_place_node
    end

    return function(pos, placer, itemstack, pointed_thing)
        if old_after_place_node then
            old_after_place_node(pos, placer, itemstack, pointed_thing)
        end

        local target = minetest.get_node(pointed_thing.under)
        local target_def = minetest.registered_nodes[target.name]

        if target_def.coppertest then
            local node = minetest.get_node(pos)
            local side = vector.subtract(pointed_thing.under, pointed_thing.above)
            node.param2 = ({2, 4, 3, 4, 1, 6, 0})[side.x+side.y*2+side.z*3+4]
            minetest.swap_node(pos, node)
        end
    end
end


function coppertest.register_wire(name, definition)
    local node_def = minetest.registered_nodes[name]
    if node_def.coppertest then
        error("node "..name.."is already registered", 2)
    end
    definition.type = "wire"

    local old_after_destruct = node_def.after_destruct or function() end
    local old_on_construct = node_def.on_construct or function() end
    local old_on_blast = node_def.on_blast or function() end

    local groups = node_def.groups or {}
    groups.coppertest = 3

    minetest.override_item(name, {
        coppertest = definition,
        groups = groups,
        connect_sides = node_def.connect_sides or sides_to_strings(definition),
        after_destruct = function(pos, oldnode)
            queue.queue(pos, 1, "power.update_neighbours", pos, oldnode)
            old_after_destruct(pos, oldnode)
        end,
        on_construct = function(pos)
            old_on_construct(pos)
            local node = minetest.get_node(pos)
            queue.queue(pos, 1, "power.update_wire", pos, node)
        end,
        on_blast = function(pos, ...)
            old_on_blast(pos, ...)
            node = minetest.get_node(pos)
            queue.queue(pos, 1, "power.update_neighbours", pos, node)
            minetest.remove_node(pos)
            return minetest.get_node_drops(name, "")
        end,
        after_place_node = get_after_place_node(node_def, definition),
    })
end


function coppertest.register_component(name, definition)
    local node_def = minetest.registered_nodes[name]
    if node_def.coppertest then
        error("node "..name.."is already registered", 2)
    end
    definition.type = "component"

    local old_on_destruct = node_def.on_destruct or function() end
    local old_on_construct = node_def.on_construct or function() end
    local old_on_blast = node_def.on_blast or function() end

    local groups = node_def.groups or {}
    groups.coppertest = 2

    minetest.override_item(name, {
        coppertest = definition,
        groups = groups,
        connect_sides = node_def.connect_sides or sides_to_strings(definition),
        on_destruct = function(pos)
            local node = minetest.get_node(pos)
            queue.queue(pos, 1, "power.set_level", pos, node, 0)
            old_on_destruct(pos)
        end,
        on_construct = function(pos)
            old_on_construct(pos)
            queue.queue(pos, 1, "power.update_component", pos)
        end,
        on_blast = function(pos, ...)
            old_on_blast(pos, ...)
            node = minetest.get_node(pos)
            queue.queue(pos, 1, "power.update_neighbours", pos, node)
            minetest.remove_node(pos)
            return minetest.get_node_drops(name, "")
        end,
        after_place_node = get_after_place_node(node_def, definition),
    })
end


--[[
    meta:mark_as_private(name or {name1, name2, ...})
    wallmounted_rotate_vertical = true changes behaviour for wallmounted rotations

    TODO: removing a node should clear all callbacks for that positions
]]--
