local helpers = {}

local rotations_facedir = {
    {0, 1, 2, 3, 4, 5}, {0, 1, 5, 4, 2, 3}, {0, 1, 3, 2, 5, 4}, {0, 1, 4, 5, 3, 2},
    {4, 5, 2, 3, 1, 0}, {4, 5, 0, 1, 2, 3}, {4, 5, 3, 2, 0, 1}, {4, 5, 1, 0, 3, 2},
    {5, 4, 2, 3, 0, 1}, {5, 4, 1, 0, 2, 3}, {5, 4, 3, 2, 1, 0}, {5, 4, 0, 1, 3, 2},
    {2, 3, 1, 0, 4, 5}, {2, 3, 5, 4, 1, 0}, {2, 3, 0, 1, 5, 4}, {2, 3, 4, 5, 0, 1},
    {3, 2, 0, 1, 4, 5}, {3, 2, 5, 4, 0, 1}, {3, 2, 1, 0, 5, 4}, {3, 2, 4, 5, 1, 0},
    {1, 0, 3, 2, 4, 5}, {1, 0, 5, 4, 3, 2}, {1, 0, 2, 3, 5, 4}, {1, 0, 4, 5, 2, 3},
}
local rotations_wallmounted = {  -- TODO support wallmounted_rotate_vertical = true
    {0, 1, 2, 3, 4, 5}, {1, 0, 3, 2, 4, 5}, {2, 3, 4, 5, 0, 1}, {3, 2, 5, 4, 0, 1},
    {4, 5, 3, 2, 0, 1}, {5, 4, 2, 3, 0, 1}, {5, 4, 2, 3, 0, 1}, {5, 4, 2, 3, 0, 1},
}
local rotations_none = {0, 1, 2, 3, 4, 5}

-- Returns a lookup table to rotate a side (0-5) by a node's rotation
function helpers.get_rotation_table(node, node_def)
    node_def = node_def or minetest.registered_nodes[node.name]

    local facedir = 0

    if node_def.paramtype2 == "wallmounted"
        or node_def.paramtype2 == "colorwallmounted" then

        local wallmounted = node.param2 % 8
        return rotations_wallmounted[wallmounted+1]

    elseif node_def.paramtype2 == "facedir"
        or node_def.paramtype2 == "colorfacedir" then

        facedir = node.param2 % 32 % 24

    elseif node_def.paramtype2 == "4dir"
        or node_def.paramtype2 == "color4dir" then

        facedir = node.param2 % 4

    elseif node_def.paramtype2 == "degrotate" then
        -- Round to the nearest 90° angle
        facedir = math.floor(node.param2 / 60 + 0.5)

    elseif node_def.paramtype2 == "colordegrotate" then
        -- Round to the nearest 90° angle
        facedir = math.floor(node.param2 / 6 + 0.5)

    else
        return rotations_none
    end

    return rotations_facedir[facedir+1]
end


-- Returns category (default=0, weak=1, read=2), type (none=0, extend=1, solid=2),
-- pos, between (node that should be solid)
-- for a side number (0-233)
function helpers.get_side_info(side, pos, rotation_table)
    local category

    category = math.floor(side / 78)
    side = side % 78

    if side >= 6 then
        side = side - 6
        local type = math.floor(side / 36) + 1
        side = side % 36
        local side_a = rotation_table[math.floor(side / 6) +1]
        local side_b = rotation_table[side % 6 +1]

        local vec_a = coppertest.vectors[side_a+1]
        local pos_a = vector.add(pos, vec_a)
        local vec_b = coppertest.vectors[side_b+1]
        local pos_c = vector.add(pos_a, vec_b)

        return category, type, pos_c, pos_a
    else
        side = rotation_table[side+1]
        local vec = coppertest.vectors[side+1]
        return category, 0, vector.add(pos, vec)
    end
end


-- Returns true if node at pos can transfer power
function helpers.is_node_solid(pos)
    local node = minetest.get_node(pos)
    local def = minetest.registered_nodes[node.name]

    if def and (def.groups.coppertest_solid or
        (def.drawtype == "normal" and not def.groups.coppertest_not_solid)
    ) then
        return true
    end
    return false
end


-- Return a new table containing items from all tables
function helpers.concat_tables(...)
    local output = {}

    for i, tbl in ipairs({...}) do
        for i, v in ipairs(tbl) do
            table.insert(output, v)
        end
    end
    return output
end

-- Round a node position to the nearest block position.
function helpers.to_block_pos(pos)
    return vector.new(
        math.floor(pos.x/16)*16,
        math.floor(pos.y/16)*16,
        math.floor(pos.z/16)*16
    )
end

-- Check if a table contains no items
function helpers.table_is_empty(t)
    for i, v in pairs(t) do
        return false
    end
    return true
end

return helpers
