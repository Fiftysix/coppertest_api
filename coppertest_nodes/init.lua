local queue = coppertest.require("queue.lua")
local power = coppertest.require("power.lua")
local helpers = coppertest.require("helpers.lua")

local modname = minetest.get_current_modname()

local rotate_facedir = {
    12, 9,  18, 7,    13, 1,  17,  21,  15, 23, 19, 3,
    20, 10, 2,  6,    0,  8,  22,  4,   16, 11, 14, 5,
}
--[[
local flip_facedir = {
    20, 23, 22, 21,   10, 9,  8,  11,   6,  5,  4,  7,
    16, 19, 18, 17,   12, 15, 14, 13,   0,  3,  2,  1,
}
]]--


-- Wire
for idx=0, coppertest.max_power do
    minetest.register_node(modname..":wire_"..idx, {
        description = "Copper Wire",
        groups = {
            cracky = 3, oddly_breakable_by_hand = 3,
            not_in_creative_inventory = (idx>0 and 1 or 0),
        },

        drawtype = "nodebox",
        node_box = {
            type = "connected",
            fixed          = {-3/16, -3/16, -3/16,  3/16,  3/16,  3/16},

            connect_right  = { 2/16, -2/16, -2/16,  8/16,  2/16,  2/16},
            connect_left   = {-2/16, -2/16, -2/16, -8/16,  2/16,  2/16},

            connect_top    = {-2/16,  2/16, -2/16,  2/16,  8/16,  2/16},
            connect_bottom = {-2/16, -2/16, -2/16,  2/16, -8/16,  2/16},

            connect_back   = {-2/16, -2/16,  2/16,  2/16,  2/16,  8/16},
            connect_front  = {-2/16, -2/16, -2/16,  2/16,  2/16, -8/16},

        },

        connects_to = {"group:coppertest"},

        paramtype = "light",
        paramtype2 = "color",

        walkable = false,
        sunlight_propagates = true,
        drop = modname..":wire_0",

        tiles = {
            "coppertest_wire.png", "coppertest_wire.png",
            "coppertest_wire.png^(coppertest_numerals.png^[verticalframe:16:"..idx..")",
        },

        palette = "coppertest_power_levels.png",
    })
    coppertest.register_wire(modname..":wire_"..idx, {
        input_sides = coppertest.sides.all,
        output_sides = coppertest.sides.all,
        on_power_change = function(pos, node, level, strength)
            node.name = modname..":wire_"..level
            node.param2 = strength / coppertest.max_power * 255
            minetest.swap_node(pos, node)
        end,
    })
end


-- Diode
minetest.register_node(modname..":diode", {
    description = "Copper Wire Diode",
    groups = {cracky = 3, oddly_breakable_by_hand = 3},

    drawtype = "nodebox",
    node_box = {
        type = "fixed",
        fixed = {
            {-2/16, -2/16, -8/16,  2/16,  2/16,  8/16},
            {-3/16, -2/16, -3/16,  3/16,  2/16,  3/16},
        }
    },

    selection_box = {
        type = "fixed",
        fixed = {
            {-3/16, -2/16, -3/16,  3/16,  2/16,  3/16},

            {-2/16, -2/16,  3/16,  2/16,  2/16,  8/16},
            {-2/16, -2/16, -3/16,  2/16,  2/16, -8/16},
        }
    },

    paramtype = "light",
    paramtype2 = "facedir",

    walkable = false,
    sunlight_propagates = true,
    drop = modname..":diode",

    tiles = {
        "coppertest_diode_straight.png",
        "coppertest_diode_straight.png^[transformFY",
        "coppertest_diode_side.png", "coppertest_diode_side.png",
        "coppertest_copper.png", "coppertest_copper.png"
    },

    on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)
        node.name = modname..":diode_corner"
        minetest.swap_node(pos, node)
        queue.queue(pos, 1, "power.update_wire", pos, node)
    end,
})
coppertest.register_wire(modname..":diode", {
    input_sides = {coppertest.sides.front, coppertest.sides.weak_solid_front_front},
    output_sides = {coppertest.sides.back, coppertest.sides.weak_solid_back_back},
})

minetest.register_node(modname..":diode_corner", {
    description = "Copper Wire Diode",
    groups = {cracky = 3, oddly_breakable_by_hand = 3, not_in_creative_inventory = 1},

    drawtype = "nodebox",
    node_box = {
        type = "fixed",
        fixed = {
            {-3/16, -2/16, -3/16,  3/16,  2/16,  3/16},

            {-3/16, -2/16, -2/16, -8/16,  2/16,  2/16},
            {-2/16, -2/16,  3/16,  2/16,  2/16,  8/16},
        }
    },

    selection_box = {
        type = "fixed",
        fixed = {
            {-3/16, -2/16, -3/16,  3/16,  2/16,  3/16},

            {-3/16, -2/16, -2/16, -8/16,  2/16,  2/16},
            {-2/16, -2/16,  3/16,  2/16,  2/16,  8/16},
        }
    },

    paramtype = "light",
    paramtype2 = "facedir",

    walkable = false,
    sunlight_propagates = true,
    drop = modname..":diode",

    tiles = {
        "coppertest_diode.png", "coppertest_diode.png^[transformFY",
        "coppertest_diode_side.png", "coppertest_copper.png",
        "coppertest_copper.png", "coppertest_diode_side.png",
    },

    on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)
        local param2_base = math.floor(node.param2/32)*32

        local facedir = node.param2 % 32 % 24
        local new_facedir = rotate_facedir[facedir+1]

        node.param2 = param2_base + new_facedir

        if new_facedir <= 4 or new_facedir == 6 then
            node.name = modname..":diode"
        end

        minetest.swap_node(pos, node)
        queue.queue(pos, 1, "power.update_wire", pos, node)
    end,
})
coppertest.register_wire(modname..":diode_corner", {
    input_sides = {coppertest.sides.right, coppertest.sides.weak_solid_right_right},
    output_sides = {coppertest.sides.back, coppertest.sides.weak_solid_back_back},
})


-- Timer
minetest.register_node(modname..":timer", {
    description = "Copper Timer",
    groups = {cracky = 3, oddly_breakable_by_hand = 3},

    drawtype = "nodebox",
    node_box = {
        type = "fixed",
        fixed = {
            {-4/16, -2/16, -4/16,  4/16,  2/16,  4/16},
            {-2/16, -2/16, -8/16,  2/16,  2/16, -4/16},
            {-2/16, -2/16,  4/16,  2/16,  2/16,  8/16},

            {-1/16, -3/16, -4/16,  1/16,  3/16,  4/16},
            {-4/16, -3/16, -1/16,  4/16,  3/16,  1/16},
            {-3/16, -3/16, -3/16,  3/16,  3/16,  3/16},
        }
    },

    selection_box = {
        type = "fixed",
        fixed = {
            {-4/16, -2/16, -4/16,  4/16,  2/16,  4/16},

            {-2/16, -2/16,  4/16,  2/16,  2/16,  8/16},
            {-2/16, -2/16, -4/16,  2/16,  2/16, -8/16},
        }
    },

    tiles = {
        "coppertest_timer.png",     "coppertest_timer.png^[transformFY",
        "coppertest_timer_side.png", "coppertest_timer_side.png",
        "coppertest_timer_side.png", "coppertest_timer_side.png",
    },
    paramtype = "light",
    paramtype2 = "facedir",

    walkable = false,
    sunlight_propagates = true,

})
coppertest.register_component(modname..":timer", {
    input_sides = {coppertest.sides.front, coppertest.sides.solid_front_front},
    output_sides = helpers.concat_tables(
        {coppertest.sides.back, coppertest.sides.solid_back_back},
        coppertest.sides.read_all, coppertest.sides.read_solid_all_extend
    ),
    on_power_change = function(pos, node, level)
        local meta = minetest.get_meta(pos)

        local timer_queued = meta:get_int("coppertest_timer_is_queued")
        if timer_queued == 1 then
            meta:set_int("coppertest_timer_next_level", level)
            return
        end

        local current_level = meta:get_int("coppertest_output_level")
        local last_tick = meta:get_int("coppertest_timer_tick")

        local delay = math.max(
            last_tick - coppertest.tick_idx + current_level,
            level
        )

        meta:set_int("coppertest_timer_next_level", -1)
        meta:set_int("coppertest_timer_is_queued", 1)
        queue.queue(pos, delay, "node.timer_callback", pos, node, level)
    end,

    timer_callback = function(pos, node, level)
        local meta = minetest.get_meta(pos)
        local next_level = meta:get_int("coppertest_timer_next_level")

        if next_level ~= -1 then
            meta:set_int("coppertest_timer_next_level", -1)
            local delay = math.max(level, next_level)
            queue.queue(pos, delay, "node.timer_callback", pos, node, next_level)

        else
            meta:set_int("coppertest_timer_is_queued", 0)
        end

        meta:set_int("coppertest_timer_tick", coppertest.tick_idx)
        power.set_level(pos, node, level)
    end,
})


-- Calculator
minetest.register_node(modname..":calculator", {
    description = "Copper Calculator",
    groups = {cracky = 3, oddly_breakable_by_hand = 3},

    drawtype = "nodebox",
    node_box = {
        type = "fixed",
        fixed = {
            {-2/16, -2/16, -8/16,  2/16,  2/16,  8/16},
            {-8/16, -2/16, -2/16,  8/16,  2/16,  2/16},
            {-4/16, -2/16, -4/16,  4/16,  2/16,  4/16},

            {-6/16, -3/16,  0   , -3/16,  3/16,  1/16},
            {-5/16, -3/16, -1/16, -4/16,  3/16,  2/16},

            { 6/16, -3/16,  0   ,  3/16,  3/16,  1/16},

            {-1/16, -3/16,  7/16,  1/16,  3/16,  6/16},
            {-1/16, -3/16,  5/16,  1/16,  3/16,  4/16},
        }
    },

    selection_box = {
        type = "fixed",
        fixed = {
            {-4/16, -2/16, -4/16,  4/16,  2/16,  4/16},

            {-2/16, -2/16,  4/16,  2/16,  2/16,  8/16},
            {-2/16, -2/16, -4/16,  2/16,  2/16, -8/16},

            { 4/16, -2/16, -2/16,  8/16,  2/16,  2/16},
            {-4/16, -2/16, -2/16, -8/16,  2/16,  2/16},
        }
    },

    tiles = {
        "coppertest_calculator.png", "coppertest_calculator.png^[transformFY",
        "coppertest_calculator_side.png", "coppertest_calculator_side.png",
        "coppertest_calculator_side.png", "coppertest_calculator_side.png",
    },
    paramtype = "light",
    paramtype2 = "facedir",

    walkable = false,
    sunlight_propagates = true,

    on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)
        local facedir = node.param2 % 32 % 24
        local param2_base = math.floor(node.param2/32)*32
        node.param2 = param2_base + rotate_facedir[facedir+1]
        minetest.swap_node(pos, node)
        queue.queue(pos, 1, "power.update_component", pos, node)
    end,

})
coppertest.register_component(modname..":calculator", {
    input_sides = {
        coppertest.sides.front, coppertest.sides.solid_front_front,
        coppertest.sides.right, coppertest.sides.solid_right_right,
        coppertest.sides.left, coppertest.sides.solid_left_left,
    },
    output_sides = {
        coppertest.sides.back, coppertest.sides.solid_back_back,

        coppertest.sides.read_front, coppertest.sides.read_solid_front_front,
        coppertest.sides.read_right, coppertest.sides.read_solid_right_right,
        coppertest.sides.read_left, coppertest.sides.read_solid_left_left,
    },
    on_power_change = function(pos, node, level, strength, changed, side_inputs)
        local base = math.max(side_inputs[1].level, side_inputs[2].level)
        local add = math.max(side_inputs[3].level, side_inputs[4].level)
        local subtract = math.max(side_inputs[5].level, side_inputs[6].level)

        local result = base+add-subtract
        result = math.max(0, math.min(result, coppertest.max_power))

        queue.queue(pos, 1, "power.set_level", pos, node, result)
    end,
})


-- Reader
minetest.register_node(modname..":reader", {
    description = "Copper Reader",
    groups = {cracky = 3, oddly_breakable_by_hand = 3},

    drawtype = "nodebox",
    node_box = {
        type = "fixed",
        fixed = {
            {-3/16, -2/16, -3/16,  3/16,  2/16,  3/16},
            {-2/16, -2/16, -6/16,  2/16,  2/16,  8/16},
            {-5/16, -5/16, -6/16,  5/16,  5/16, -8/16},

            {-2/16, -3/16, -1/16,  2/16,  3/16,  2/16},
            {-3/16, -3/16,  0   ,  3/16,  3/16,  1/16},
        }
    },

    selection_box = {
        type = "fixed",
        fixed = {
            {-3/16, -2/16, -3/16,  3/16,  2/16,  3/16},

            {-2/16, -2/16, -6/16,  2/16,  2/16, -3/16},
            {-2/16, -2/16,  3/16,  2/16,  2/16,  8/16},

            {-5/16, -5/16, -6/16,  5/16,  5/16, -8/16},
        }
    },

    tiles = {
        "coppertest_reader.png", "coppertest_reader.png^[transformFY",
        "coppertest_reader_side.png^[transformFX", "coppertest_reader_side.png",
        "coppertest_reader_back.png", "coppertest_reader_face.png",
    },
    paramtype = "light",
    paramtype2 = "facedir",

    walkable = false,
    sunlight_propagates = true,

    on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)
        node.name = modname..":reader_conditional"
        minetest.swap_node(pos, node)
        queue.queue(pos, 1, "power.update_component", pos, node)
    end,
})
coppertest.register_component(modname..":reader", {
    input_sides = {
        coppertest.sides.front, coppertest.sides.read_front,
        coppertest.sides.solid_front_front, coppertest.sides.read_solid_front_front,
    },
    output_sides = {
        coppertest.sides.back, coppertest.sides.solid_back_back,

        coppertest.sides.read_up, coppertest.sides.read_solid_up_up,
        coppertest.sides.read_down, coppertest.sides.read_solid_down_down,
        coppertest.sides.read_left, coppertest.sides.read_solid_left_left,
        coppertest.sides.read_right, coppertest.sides.read_solid_right_right,
    },

    on_power_change = function(pos, node, level, strength, changed, side_inputs)
        queue.queue(pos, 1, "power.set_level", pos, node, strength)
    end,

})

minetest.register_node(modname..":reader_conditional", {
    description = "Conditional Copper Reader",
    groups = {cracky = 3, oddly_breakable_by_hand = 3, not_in_creative_inventory = 1},

    drawtype = "nodebox",
    node_box = {
        type = "fixed",
        fixed = {
            {-3/16, -2/16, -3/16,  3/16,  2/16,  3/16},
            {-2/16, -2/16, -6/16,  2/16,  2/16,  8/16},
            {-5/16, -5/16, -6/16,  5/16,  5/16, -8/16},

            {-8/16, -1/16, -1/16, -3/16,  1/16,  1/16},

            {-2/16, -3/16, -1/16,  2/16,  3/16,  2/16},
            {-3/16, -3/16,  0   ,  3/16,  3/16,  1/16},
        }
    },

    selection_box = {
        type = "fixed",
        fixed = {
            {-3/16, -2/16, -3/16,  3/16,  2/16,  3/16},

            {-2/16, -2/16, -6/16,  2/16,  2/16, -3/16},
            {-2/16, -2/16,  3/16,  2/16,  2/16,  8/16},

            {-5/16, -5/16, -6/16,  5/16,  5/16, -8/16},

            {-8/16, -1/16, -1/16, -3/16,  1/16,  1/16},
        }
    },

    tiles = {
        "coppertest_reader.png", "coppertest_reader.png^[transformFY",
        "coppertest_reader_side.png^[transformFX", "coppertest_reader_side.png",
        "coppertest_reader_back.png", "coppertest_reader_face.png",
    },
    paramtype = "light",
    paramtype2 = "facedir",

    walkable = false,
    sunlight_propagates = true,
    drop = modname..":reader",

    on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)
        local param2_base = math.floor(node.param2/32)*32

        local facedir = node.param2 % 32 % 24
        local new_facedir = rotate_facedir[facedir+1]

        node.param2 = param2_base + new_facedir
        if new_facedir <= 4 or new_facedir == 6 then
            node.name = modname..":reader"
        end

        minetest.swap_node(pos, node)
        queue.queue(pos, 1, "power.update_component", pos, node)
    end,
})
coppertest.register_component(modname..":reader_conditional", {
    input_sides = {
        coppertest.sides.right,
        coppertest.sides.front, coppertest.sides.read_front,
        coppertest.sides.solid_front_front, coppertest.sides.read_solid_front_front,
    },
    output_sides = {
        coppertest.sides.back, coppertest.sides.solid_back_back,

        coppertest.sides.read_up, coppertest.sides.read_solid_up_up,
        coppertest.sides.read_down, coppertest.sides.read_solid_down_down,
        coppertest.sides.read_left, coppertest.sides.read_solid_left_left,
    },

    on_power_change = function(pos, node, level, strength, changed, side_inputs)
        if side_inputs[1].level ~= 0 then
            for idx = 2, 5 do
                strength = side_inputs[idx].strength
                if strength ~= 0 then break end
            end
            queue.queue(pos, 1, "power.set_level", pos, node, strength)
        else
            queue.queue(pos, 1, "power.set_level", pos, node, 0)
        end
    end,
})


-- Lamp
minetest.register_node(modname..":lamp_off", {
    description = "Copper Lamp",
    tiles = {"coppertest_lamp_off.png"},
    groups = {cracky = 3, oddly_breakable_by_hand = 3},
})
coppertest.register_component(modname..":lamp_off", {
    on_power_change = function(pos, node, level)
        if level == 0 then
            node.name = modname..":lamp_off"
        else
            node.name = modname..":lamp_on_"..level
        end
        minetest.swap_node(pos, node)
        coppertest.set_level(pos, node, level)
    end,
    input_sides = helpers.concat_tables(
        coppertest.sides.all, coppertest.sides.solid_all_extend
    ),
})

for idx=1, coppertest.max_power do
    minetest.register_node(modname..":lamp_on_"..idx, {
        description = "Copper Lamp (on)",
        tiles = {"coppertest_lamp_on.png"},
        groups = {cracky = 3, oddly_breakable_by_hand = 3, not_in_creative_inventory=1},
        paramtype = "light",
        drop = modname..":lamp_off",
        light_source = math.floor(idx/coppertest.max_power*(minetest.LIGHT_MAX-2)+2.5),
    })
    coppertest.register_component(modname..":lamp_on_"..idx, {
        on_power_change = function(pos, node, level)
            local new_node = {param1=node.param1}
            if level == 0 then
                new_node.name = modname..":lamp_off"
            else
                new_node.name = modname..":lamp_on_"..level
            end
            minetest.swap_node(pos, new_node)
            coppertest.set_level(pos, node, level)
        end,
        input_sides = helpers.concat_tables(
            coppertest.sides.all, coppertest.sides.solid_all_extend
        ),
        output_sides = helpers.concat_tables(
            coppertest.sides.read_all, coppertest.sides.read_solid_all_extend
        ),
    })
end


-- Switch
minetest.register_node(modname..":switch_off", {
    description = "Copper Switch",
    tiles = {
        "coppertest_switch_side.png", "coppertest_switch_side.png",
        "coppertest_switch_side.png", "coppertest_switch_side.png",
        "coppertest_dial_side.png", "coppertest_switch_off.png",
    },
    groups = {cracky = 3, oddly_breakable_by_hand = 3},

    drawtype = "nodebox",
    node_box = {
        type = "fixed",
        fixed = {
            { 1/16, -5/16, 6/16,  5/16,  5/16, 8/16},
            {-5/16,  3/16, 6/16,  5/16,  5/16, 8/16},
            {-5/16, -5/16, 6/16, -1/16,  5/16, 8/16},
            {-5/16, -5/16, 6/16,  5/16, -3/16, 8/16},

            {-1/16, -3/16, 6.5/16,1/16,  3/16, 8/16},

            {-3/16, -3/16, 4/16,  3/16, -1/16, 8/16},
        }
    },

    selection_box = {
        type = "fixed",
        fixed = {
            {-5/16, -5/16, 6/16,  5/16,  5/16, 8/16},
            {-3/16, -3/16, 4/16,  3/16, -1/16, 7/16},
        }
    },

    paramtype = "light",
    paramtype2 = "facedir",

    walkable = false,
    sunlight_propagates = true,

    on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)
        node.name = modname..":switch_on"
        minetest.swap_node(pos, node)
        coppertest.set_level(pos, node, coppertest.max_power)
    end,
})
coppertest.register_component(modname..":switch_off", {
    output_sides = {coppertest.sides.back, coppertest.sides.solid_back_back},
})

minetest.register_node(modname..":switch_on", {
    description = "Copper Switch (on)",
    tiles = {
        "coppertest_switch_side.png", "coppertest_switch_side.png",
        "coppertest_switch_side.png", "coppertest_switch_side.png",
        "coppertest_dial_side.png", "coppertest_switch_on.png",
    },
    groups = {cracky = 3, oddly_breakable_by_hand = 3, not_in_creative_inventory=1},

    drawtype = "nodebox",
    node_box = {
        type = "fixed",
        fixed = {
            { 1/16, -5/16, 6/16,  5/16,  5/16, 8/16},
            {-5/16,  3/16, 6/16,  5/16,  5/16, 8/16},
            {-5/16, -5/16, 6/16, -1/16,  5/16, 8/16},
            {-5/16, -5/16, 6/16,  5/16, -3/16, 8/16},

            {-1/16, -3/16, 6.5/16,1/16,  3/16, 8/16},

            {-3/16,  1/16, 4/16,  3/16,  3/16, 8/16},
        }
    },

    selection_box = {
        type = "fixed",
        fixed = {
            {-5/16, -5/16, 6/16,  5/16,  5/16, 8/16},
            {-3/16,  1/16, 4/16,  3/16,  3/16, 7/16},
        }
    },

    paramtype = "light",
    paramtype2 = "facedir",

    walkable = false,
    sunlight_propagates = true,
    drop = modname..":switch_off",

    on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)
        minetest.swap_node(pos, {
            name=modname..":switch_off", param1=node.param1, param2=node.param2
        })
        coppertest.set_level(pos, node, 0)
    end,
})
coppertest.register_component(modname..":switch_on", {
    output_sides = {coppertest.sides.back, coppertest.sides.solid_back_back},
})


-- Dial
for idx=0, coppertest.max_power do
    minetest.register_node(modname..":dial_"..idx, {
        description = "Copper Dial",
        tiles = {
            "coppertest_dial_side.png","coppertest_dial_side.png",
            "coppertest_dial_side.png", "coppertest_dial_side.png",
            "coppertest_dial_side.png",
            "coppertest_dial.png^(coppertest_numerals.png^[verticalframe:16:"..idx.."^[invert:r)",
        },
        groups = {cracky = 3, oddly_breakable_by_hand = 3,
            not_in_creative_inventory = (idx>0 and 1 or 0),
        },

        drawtype = "nodebox",
        node_box = {
            type = "fixed",
            fixed = {
                {-7/16, -3/16, 6/16, -2/16,  4/16, 8/16},
                { 2/16, -3/16, 6/16,  7/16,  4/16, 8/16},

                {-2/16,  3/16, 6/16,  2/16,  4/16, 8/16},
                {-2/16, -3/16, 6/16,  2/16, -2/16, 8/16},

                {-2/16,  3/16, 6.5/16,2/16, -2/16, 8/16},

                {-6/16, -2/16, 5/16, -3/16,  3/16, 6/16},
                { 3/16, -2/16, 5/16,  6/16,  3/16, 6/16},
            }
        },

        selection_box = {
            type = "fixed",
            fixed = {
                {-6/16, -2/16, 5/16, -3/16,  3/16, 6/16}, -- minus
                {-7/16, -3/16, 6/16,  7/16,  4/16, 8/16},
                { 3/16, -2/16, 5/16,  6/16,  3/16, 6/16}, -- plus
            }
        },

        paramtype = "light",
        paramtype2 = "facedir",

        walkable = false,
        sunlight_propagates = true,
        drop = modname..":dial_0",

        on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)
            local eye_pos = clicker:get_pos()
            eye_pos.y = eye_pos.y + clicker:get_properties().eye_height
            local reach = vector.distance(eye_pos, pointed_thing.under) + 1
            local target_offset = vector.multiply(clicker:get_look_dir(), reach)
            local target_pos = vector.add(eye_pos, target_offset)

            local ray = minetest.raycast(eye_pos, target_pos, false, false)
            local box_id = ray:next().box_id

            local level = minetest.get_meta(pos):get_int("coppertest_output_level")
            local new_level = (level+box_id-2)%(coppertest.max_power+1)

            node.name = modname..":dial_"..new_level
            minetest.swap_node(pos, node)
            coppertest.set_level(pos, node, new_level)
        end,
    })
    coppertest.register_component(modname..":dial_"..idx, {
        output_sides = {coppertest.sides.back, coppertest.sides.solid_back_back},
    })
end


-- Button
minetest.register_node(modname..":button_off", {
    description = "Copper Button",
    tiles = {
        "coppertest_switch_side.png", "coppertest_switch_side.png",
        "coppertest_switch_side.png", "coppertest_switch_side.png",
        "coppertest_dial_side.png", "coppertest_button_off.png",
    },
    groups = {cracky = 3, oddly_breakable_by_hand = 3},

    drawtype = "nodebox",
    node_box = {
        type = "fixed",
        fixed = {
            {-5/16, -5/16, 6/16,  5/16,  5/16, 8/16},
            {-3/16, -3/16, 4/16,  3/16,  3/16, 8/16},
        }
    },

    selection_box = {
        type = "fixed",
        fixed = {
            {-5/16, -5/16, 6/16,  5/16,  5/16, 8/16},
            {-3/16, -3/16, 4/16,  3/16,  3/16, 7/16},
        }
    },

    paramtype = "light",
    paramtype2 = "facedir",

    walkable = false,
    sunlight_propagates = true,

    on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)
        node.name = modname..":button_on"
        minetest.swap_node(pos, node)
        coppertest.set_level(pos, node, coppertest.max_power)

        local playername = clicker:get_player_name()
        queue.queue(pos, 5, "node.rightclick_check_callback", pos, playername)
    end,
})
coppertest.register_component(modname..":button_off", {
    output_sides = {coppertest.sides.back, coppertest.sides.solid_back_back},
})

minetest.register_node(modname..":button_on", {
    description = "Copper Button (on)",
    tiles = {
        "coppertest_switch_side.png", "coppertest_switch_side.png",
        "coppertest_switch_side.png", "coppertest_switch_side.png",
        "coppertest_dial_side.png", "coppertest_button_on.png",
    },
    groups = {cracky = 3, oddly_breakable_by_hand = 3, not_in_creative_inventory=1},

    drawtype = "nodebox",
    node_box = {
        type = "fixed",
        fixed = {
            {-5/16, -5/16, 6/16,  5/16,  5/16, 8/16},
            {-3/16, -3/16, 5.5/16,3/16,  3/16, 8/16},
        }
    },

    selection_box = {
        type = "fixed",
        fixed = {
            {-5/16, -5/16, 6/16,  5/16,  5/16, 8/16},
            {-3/16, -3/16, 5.5/16,3/16,  3/16, 7/16},
        }
    },

    paramtype = "light",
    paramtype2 = "facedir",

    walkable = false,
    sunlight_propagates = true,
    drop = modname..":button_off",
    on_rightclick = function(pos, node, clicker, itemstack, pointed_thing) end,
})
coppertest.register_component(modname..":button_on", {
    output_sides = {coppertest.sides.back, coppertest.sides.solid_back_back},

    rightclick_check_callback = function(pos, playername)
        local clicker = minetest.get_player_by_name(playername)
        if clicker then
            local rightclick = clicker:get_player_control().place

            if rightclick then
                queue.queue(pos, 2, "node.rightclick_check_callback", pos, playername)
                return
            end
        end

        local node = minetest.get_node(pos)
        node.name = modname..":button_off"
        minetest.swap_node(pos, node)
        coppertest.set_level(pos, node, 0)
    end,
})
